import time
import logging

def timed(func):
    def wrapper(*arg, **kw):
        t_start = time.time()
        res = func(*arg, **kw)
        t_end = time.time()
        logging.info("Method `{}`, time: {} ms".format(func.__name__, (t_end-t_start) * 1000))
        return res
    return wrapper
